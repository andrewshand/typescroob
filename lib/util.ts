namespace Util {
    
    export function playSound(url: string) {
        $('audio').remove();
        const audio = $('<audio>').attr({
            src: url
        }).appendTo('body')[0] as HTMLAudioElement;
        audio.play();
    }
}