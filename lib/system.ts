interface IncomingEvent {
    volume: number
    eventType: string
}

namespace System {
    let floodsSoFar: number = 0;
    export function soundTheFloodAlarm() {
        log('Flood alarm sounded!!!');
        floodsSoFar++;
    }

    let activities = [
        'eating',
        'munching',
        'whistling',
        'crying',
        'scooting',
        'working for free',
        'stomping',
        'whaling',
        'lowering morale',
        'boosting morale',
        'taking one for the team'
    ];
    export function spyOnEmployee(employee: string) {
        log(`Focusing CCTV on ${employee}`);
        const activity = activities[Math.floor(Math.random() * activities.length)];
        setTimeout(() => log(`They are ${activity} (???)`), 2000);
    }

    export function ding() {
        log('ding');
    }

    export function log(message: string) {
        $('.console').append(`<div>${message}</div>`);
    }

    let suspicionLevel = 1;
    export function raiseSuspicionLevels(reason: string) {
        suspicionLevel++;
        log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        log('!! suspicion level raised !!')
        log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        if (suspicionLevel > 1) {
            log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            log('!! SUSPICION LEVEL REACHING CRITICAL LEVELS !!')
            log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        }
    }

    export function backupEvents(events: IncomingEvent[]) {
        log("Phew. Loud events backed up. Thanks. I can rest easy tonight.");
    }

    export function connect(onEvent: (event: IncomingEvent) => void) {
        const socket = new WebSocket('ws://192.168.1.31:8080');
        socket.onopen = event => log('Connected!');
        socket.onmessage = event => onEvent(JSON.parse(event.data));
    }
}